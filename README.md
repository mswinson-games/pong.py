# pong

basic implementation of pong from Coursera Course 'Introduction To Python'


## Installation

    git clone http://bitbucket.org/mswinson-games/pong.py

## Usage

    cd pong.py
    docker-compose up
    
    navigate to http://<DOCKER_MACHINE_IP>:8080/vnc.html and hit 'Connect'


## Controls

    player 1 (left)  - up   : q
    player 1 (left)  - down : a
    player 2 (right) - up   : up arrow
    player 2 (right) - down : down arrow 


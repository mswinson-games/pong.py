FROM mswinson/ubuntu-buildimage-sdl:16.04

RUN apt-get update -y && apt-get upgrade -y && \
    apt-get install -y python python-pip && \
    pip install pygame

ADD . /var/app
WORKDIR /var/app
RUN pip install -r requirements.txt
CMD ./pong
